# _*_coding utf-8_*_

from flask import Flask, render_template, request, redirect, url_for, session

from pymongo import MongoClient


app = Flask(__name__)
app.secret_key = "this looks secure enought!"
client = MongoClient('localhost', 27017)
db = client['testflask']
collection = db.person


@app.route("/", methods=['GET', 'POST'])
def index():
    if 'user' in session:
        names = []
        for man in db.person.find():
            names.append({"name": man['name'], "family": man['family']})
        return render_template('index.html', title= "index", names= names)
    else:
        return redirect(url_for('login'))


@app.route("/login", methods=['GET'])
def login():
    return render_template("login.html")


@app.route("/logout", methods=['GET'])
def logout():
    session.pop('user')
    return redirect(url_for('login'))


@app.route("/dologin", methods=['POST'])
def dologin():
    username = request.form['username']
    password = request.form['password']
    if username == "senaps" and password == "12345":
        session['user'] = username
        return redirect(url_for('index'))
    else:
        return redirect(url_for('login'))


@app.route("/insert", methods=['GET', 'POST'])
def insert():
    if 'user' in session:
        if request.method == 'POST':
            person = {"name": request.form['name'],
                      "family": request.form['family']}
            result = db.person.insert(person)
            if result:
                return redirect(url_for('index'))
            else:
                return redirect(url_for('insert'))
        else:
            return render_template('insert.html', title="insert")
    else:
        return redirect(url_for('login'))


@app.route("/edit/<name>", methods=['GET', 'POST'])
def edit(name):
    if 'user' in session:
        if request.method == 'POST':
            new_name = request.form['name']
            new_family = request.form['family']
            updated = db.person.update({"name": request.form['name']},
                                       {"name": new_name,
                                        "family": new_family})
            if updated:
                return redirect(url_for('index'))
            else:
                return "something wrong!"
        else:
            names = []
            for man in db.person.find({"name": name}):
                names.append({"name": man['name'], "family": man['family']})
                return render_template('edit.html',
                                       names = names,
                                       title="edit the name!")
    else:
        return redirect(url_for('login'))


@app.route("/remove/<name>", methods=['GET'])
def remove(name):
    if 'user' in session:
        removed = db.person.remove({"name": name})
        if removed:
            return redirect(url_for('index'))
        else:
            return "something wrong!"
    

if __name__ == '__main__':
    app.run(debug=True)
